#+Title:     Schreinerei Meier, Kontakt
#+Author:    Schreinerei Meier Team
#+Email:     (concat "test" at-sign "example.com")
#+Description: Schreinerei Meier im Internet
#+Keywords: Schreinerei Meier, Kontakt
#+Language:  de
#+Options:   num:nil toc:nil
#+HTML_CONTAINER: article

* Impressum
  :PROPERTIES:
  :CUSTOM_ID: impressum
  :END:

Angaben gem. § 5 TMG

- Betreiber und Kontakt: :: Schreinerei Meier\\
  Möbelstr. 1\\
  D-12345\\
  Musterstadt\\
  Germany\\
- Tel: :: +49 1234 5678
- Fax: :: +49 1234 5679
- E-Mail-Adresse: :: [[mailto:test@example.com][test@example.com]]

- Vertretung: :: Die Schreinerei Meier wird rechtlich vertreten durch
  Herrn Harry Meier

*** Berufsspezifische Angaben:
    :PROPERTIES:
    :CUSTOM_ID: berufsspezifische-angaben
    :END:

Berufsbezeichnung: k. A.

Zuständige Kammer: IHK Bayern

Verliehen in/durch: k. A.

Folgende berufsrechtliche Regelungen finden Anwendung: k. A.\\
Diese Regelungen können Sie einsehen unter: k. A.

Zuständige Aufsichtsbehörde: Finanzamt Musterstadt

Register und Registernummer: 999999 999999

Umsatzsteuer-ID: 999999 999999

Verantwortlicher für journalistisch-redaktionelle Inhalte gem. § 55 II
RstV:\\
Hans Jürgen Mustermann

Imprints hints: [[https://www.checkdomain.net/en/wordpress/imprint/][/How to create your imprint in WordPress/]] at
=checkdomain.net=.

** Kontakt
   :PROPERTIES:
   :CUSTOM_ID: kontakt
   :END:

- E-Mail-Adresse: :: test@example.com
- Tel: :: +49 1234 5678
- Fax: :: +49 1234 5679
