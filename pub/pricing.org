#+Title:     Schreinerei Meier, Preistabelle
#+Author:    Schreinerei Meier Team
#+Email:     (concat "test" at-sign "example.com")
#+Description: Schreinerei Meier im Internet
#+Keywords: Schreinerei Meier, Preise
#+Language:  de
#+Options:   num:nil toc:nil


* Unsere nur zu Beispielzwecken erdachten Phantasie-Preise
  :PROPERTIES:
  :CUSTOM_ID: unsere-nur-zu-beispielzwecken-erdachten-phantasie-preise
  :END:

| Produkt   | Preis   |
|-----------+---------|
| Tisch     | 50 €    |
| Schrank   | 70 €    |
| Bett      | 100 €   |
