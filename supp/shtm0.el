(setq org-publish-project-alist
      '(("shtm0"
	 :base-directory "~/myOrgRoot/pub/shtm0/"
	 :publishing-directory "~/www/shtm0/"
	 :publishing-function org-html-publish-to-html
	 :body-only t
	 :html-doctype "html5"
	 :html-container "section" )))
